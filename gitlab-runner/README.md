# kubernetes gitlab runner

## Setting up namespace, service account and role

```
kubectl create namespace gitlab-runner

kubectl create serviceaccount gitlab-runner -n gitlab-runner

kubectl apply -f role-runner.yaml
```

### Deploy MinIO

```
helm repo add minio https://helm.min.io/

kubectl apply -f minio-standalone-pvc.yaml

helm install minio minio/minio -n gitlab-runner -f minio-values.yaml
```

GUI minio:

```
ACCESS_KEY=$(kubectl get secret minio -o jsonpath="{.data.accesskey}" | base64 --decode)
SECRET_KEY=$(kubectl get secret minio -o jsonpath="{.data.secretkey}" | base64 --decode)

echo $ACCESS_KEY
echo $SECRET_KEY

kubectl port-forward service/minio -n gitlab-runner 9000:9000
```

### Deploy the GitLab Runner

```
kubectl create secret generic s3access \
    --from-literal=accesskey="YourAccessKey" \
    --from-literal=secretkey="YourSecretKey"
```

```
helm repo add gitlab https://charts.gitlab.io
helm repo update

helm install gitlab-runner gitlab/gitlab-runner -n gitlab-runner -f gitlab-runner-values.yaml
```

deinstall:

`helm delete gitlab-runner -n gitlab-runner`
