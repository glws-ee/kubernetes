FROM golang:alpine as go

RUN apk add --update git

# build semver
RUN go get github.com/davidrjonas/semver-cli

FROM alpine:edge

ARG USER=nonroot
ENV HOME /home/$USER

# gettext: envsubst
ENV BUILD_PACKAGES \
  bash \
  ca-certificates \
  curl \
  gettext \
  git \
  jq \
  openssl \
  procps \
  yq \
  wget

# enable edge/community repo [yq]
RUN \
  echo "@main http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories

# install packages
RUN \
  apk --no-cache --update-cache --available upgrade && \
  apk add --no-cache --update-cache ${BUILD_PACKAGES} && \
  rm -rf /var/cache/apk/*

WORKDIR /usr/local/bin

# kubectl
#kustomize
# helm 3
# kubeval
RUN \
  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
  curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" | bash && \
  curl -s "https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3" | bash && \
  wget https://github.com/instrumenta/kubeval/releases/latest/download/kubeval-linux-amd64.tar.gz && \
  tar xvfzmp kubeval-linux-amd64.tar.gz && \
  chmod g=rwx /usr/local/bin/*

# semver-cli
COPY --from=go /go/bin/semver-cli /usr/local/bin/semver

# non-root container
RUN adduser -S -D -G root $USER

USER $USER
WORKDIR $HOME
